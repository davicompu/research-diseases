-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema research_diseases
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `disease`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `disease` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `overview` TEXT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `disease_synonym`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `disease_synonym` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `disease_id` INT NOT NULL,
  `synonym` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_disease_synonym_disease_idx` (`disease_id` ASC),
  CONSTRAINT `fk_disease_synonym_disease`
    FOREIGN KEY (`disease_id`)
    REFERENCES `disease` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `patient_asocciation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `patient_asocciation` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `disease_id` INT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `address` VARCHAR(200) NOT NULL,
  `phone` VARCHAR(15) NOT NULL,
  `phone_800` VARCHAR(15) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `website` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_patient_asocciations_disease1_idx` (`disease_id` ASC),
  CONSTRAINT `fk_patient_asocciations_disease1`
    FOREIGN KEY (`disease_id`)
    REFERENCES `disease` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `author`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `author` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `created_at` VARCHAR(45) NOT NULL,
  `url` TEXT NOT NULL,
  `followers_count` INT NOT NULL,
  `statuses_count` INT NOT NULL,
  `favorites_count` INT NOT NULL,
  `verified` VARCHAR(45) NOT NULL,
  `location` VARCHAR(45) NOT NULL,
  `lang` VARCHAR(10) NOT NULL,
  `description` TEXT NOT NULL,
  `time_zone` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tweet`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tweet` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `author_id` BIGINT NOT NULL,
  `disease_id` INT NOT NULL,
  `keyword` VARCHAR(100) NOT NULL,
  `created_at` VARCHAR(100) NOT NULL,
  `favorited` VARCHAR(10) NOT NULL,
  `tweet_id` VARCHAR(45) NOT NULL,
  `geo` TEXT NOT NULL,
  `text` TEXT NOT NULL,
  `source` TEXT NOT NULL,
  `retweeted` VARCHAR(10) NOT NULL,
  `location` VARCHAR(100) NOT NULL,
  `time_zone` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tweets_disease1_idx` (`disease_id` ASC),
  INDEX `fk_tweet_author1_idx` (`author_id` ASC),
  CONSTRAINT `fk_tweets_disease1`
    FOREIGN KEY (`disease_id`)
    REFERENCES `disease` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tweet_author1`
    FOREIGN KEY (`author_id`)
    REFERENCES `author` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hashtag`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hashtag` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tweet_id` BIGINT NOT NULL,
  `hashtag` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_hashtags_tweets1_idx` (`tweet_id` ASC),
  CONSTRAINT `fk_hashtags_tweets1`
    FOREIGN KEY (`tweet_id`)
    REFERENCES `tweet` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `user_mention`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user_mention` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tweet_id` BIGINT NOT NULL,
  `user` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_mention_tweet1_idx` (`tweet_id` ASC),
  CONSTRAINT `fk_user_mention_tweet1`
    FOREIGN KEY (`tweet_id`)
    REFERENCES `tweet` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `url`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `url` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tweet_id` BIGINT NOT NULL,
  `url` TEXT NOT NULL,
  `expanded_url` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_url_tweet1_idx` (`tweet_id` ASC),
  CONSTRAINT `fk_url_tweet1`
    FOREIGN KEY (`tweet_id`)
    REFERENCES `tweet` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
