from Database import *
import twitter
import time
class Twitter_Bot:
	def __init__(self):
		consumer_key='5wNE0rkAL9LTkWaQvwvAWR9jo'
		consumer_secret='KbzBOHiKa64BmMs2HDnagC1UUkwbUODalXtVqVbqWn5dk1ITXP'
		access_token_key='111409397-AJ7BOgvKB4YulLCxGK3Xlox3xAy77XBDR4y7rCdL'
		access_token_secret='SmodjDTFmsY3wnpXA25UMAh63QM84CBS56Shrd92wDU43'
		self.api = twitter.Api(
		 consumer_key=consumer_key,
		 consumer_secret=consumer_secret,
		 access_token_key=access_token_key,
		 access_token_secret=access_token_secret
		 )
	def searchKeyword(self,disease,keyword,language):
		#tweets=[]
		search = self.api.GetSearch(term=keyword,lang=language,count=100,result_type='recent')
		while(search):
			self.save_tweets(search,disease,keyword)
			#tweets.extend(search)
			last_id=int(search[-1].id)-1
			#last_date=search[-1].created_at
			last_date=self.getCreatedDate(search[-1])
			print "\nSearching '"+keyword+"' ("+str(len(search))+")"
			print "Last id: "+str(last_id)
			print "Last date "+last_date
			print "Waiting 5 seconds"
			time.sleep(5)
			search =self.api.GetSearch(term=keyword,lang=language,count=100,max_id=last_id)
			if (len(search)==0):
				print "\nLooking for more until "+last_date
				search =self.api.GetSearch(term=keyword,lang=language,count=100,max_id=last_id,until=last_date)
		#return tweets;
		#return search;
	def getUser(self,tweet):
		return tweet.user;

	def getUsername(self,tweet):
		return tweet.user.screen_name

	def getCreatedDate(self,tweet):
		return tweet.created_at
	def getCreatedDate(self,tweet):
		date_str=tweet.created_at
		date=date_str.split(" ")
		months={"Jan":"01","Feb":"02","Mar":"03","Apr":"04","May":"05","Jun":"06","Jul":"07","Aug":"08","Sep":"09","Oct":"10","Nov":"11","Dec":"12"}
		day=date[2]
		month=months[date[1]]
		year=date[5]
		return year+'-'+month+'-'+day

	def getText(self,tweet):
		return tweet.text.encode('utf-8')

	def save_tweets(self,tweets,disease,keyword):
		for tweet in tweets:
			self.save_tweet(tweet,disease,keyword)

	def save_author(self,author):
		db= Database('localhost','research_diseases','root','');
		rows=db.doSelect('name','author','WHERE id='+str(author.id))
		if len(rows)>0:
			print "Not saving author"
		else:
			print "Saving author"
			conn=db.getConnection()
			#print author
			table='author'
			description=author.description
			description=description.encode('ascii','ignore')
			descrition=description.encode('utf-8')
			location=author.location
			location=location.encode('ascii','ignore')
			location=location.encode('utf-8')
			columns="id,username,name,created_at,url,followers_count,statuses_count,favorites_count,verified,location,lang,description"
			values="'"+str(author.id)+"','"+author.screen_name+"','"+author.name+"','"+author.created_at+"','"
			values=values+conn.escape_string(author.profile_image_url.encode('utf-8'))+"','"+str(author.followers_count)+"','"+str(author.statuses_count)+"','"
			values=values+str(author.favourites_count)+"','"+str(author.verified)+"','"+conn.escape_string(location)+"','"
			values=values+author.lang+"','"+conn.escape_string(description)+"'"
			#print values
			db.doInsert(columns,table,values.encode('utf-8'));


	def save_tweet(self,tweet,disease,keyword):
		db= Database('localhost','research_diseases','root','');
		rows=db.doSelect('id','tweet','WHERE id='+str(tweet.id))
		if len(rows)==0:
			table="tweet"
			conn=db.getConnection()
			self.save_author(tweet.user)
			print "\nSaving tweet from "+tweet.user.screen_name
			#source=tweet.source.encode('utf-8')
			source=tweet.source.encode('ascii','ignore')
			source=conn.escape_string(source)
			location=str(tweet.location)
			location=location.encode('ascii','ignore')
			location=conn.escape_string(location)
			location=location.replace("'","\'")
			#text=tweet.text.encode('utf-8')
			text=tweet.text.encode('ascii','ignore')
			text=conn.escape_string(text)
			#print tweet
			columns="id,tweet_id,author_id,disease_id,"
			columns+="keyword,created_at,favorited,"
			columns+="geo,text,source,retweeted,"
			columns+="location,time_zone"
			values='"'+str(tweet.id)+'","'+str(tweet.id)+'","'+str(tweet.user.id)+'","'+str(disease)+'","'
			values+=keyword+'","'+tweet.created_at+'","'+str(tweet.favorited)+'","'
			values+=str(tweet.geo)+'","'+text+'","'+source+'","'+str(tweet.retweeted)+'","'
			values+=location+'","'+str(tweet.user.time_zone)+'"'
			#print values
			db.doInsert(columns,table,values);
		else:
			print "\nNot saving tweet, because is already saved\n"

