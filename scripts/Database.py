import MySQLdb
class Database:
	def __init__(self,hostname,database,username,password):
		self.hostname=hostname
		self.database=database
		self.username=username
		self.password=password
		self.connection = MySQLdb.connect(host=hostname, user=username, passwd=password, db=database,charset='utf8', use_unicode=False)
		self.cursor = self.connection.cursor()
	def doSelect(self,columns,table,options=""):
		query='SELECT '+columns+' FROM '+table+' '+options
		print query
		try:
	   		self.cursor.execute(query)
			data = self.cursor.fetchall()
			return data
		except:
	   		self.connection.rollback()
			self.connection.close()
	def doInsert(self,columns,table,values):

			query='INSERT INTO '+table+' ('+columns+') VALUES ('+values+')'
			print query
			try:
	   			self.cursor.execute (query)
	   			self.connection.commit()
			except:
				print "Exception";
	   			self.connection.rollback()
				self.connection.close()

	def doUpdate(self,table,identifier,field,value):
		query='UPDATE '+table+' SET '+field+'='+str(field)+' WHERE id='+str(identifier)
		try:
	   		self.cursor.execute (query)
	   		self.connection.commit()
		except:
	   		self.connection.rollback()
			self.connection.close()

	def doDelete(self,table,identifier,field):
		query='DELETE FROM '+table+' WHERE '+field+'='+str(identifier)
		try:
	   		self.cursor.execute (query)
	   		self.connection.commit()
		except:
	   		self.connection.rollback()
			self.connection.close()
	def getConnection(self):
		return self.connection
